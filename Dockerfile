FROM nginx:1.24
COPY index.html /usr/share/nginx/html
EXPOSE 80

HEALTHCHECK --interval=30s --timeout=30s --start-period=5s --retries=3 \
 CMD curl --fail http://localhost:80/ || exit 1
USER nginx
